<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <title>Temperatura</title>
</head>

<body>

    <center>
        <br>
        <h2>Monitor de temperatura</h2>
        <hr>
        <br>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                limite inferior
                <h3 style="color:red" id="minTemp">
                </h3>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <h1>Temperatura actual</h1>
                <h2 id="currTemp">
                </h2>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                limite superior

                <h3 style="color:red" id="maxTemp">
                </h3>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <h2>Temperatura promedio de hoy</h2>
        <h3 id="avgTemp">

        </h3>

        <hr>
        <br>
        <br>
        <h2>
            Historial de temperatura
        </h2>
        <br>
        <br>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
        </div>
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
            <table id="history" class="table">
                <thead>
                    <th class="text-center">
                        temperatura
                    </th>
                    <th class="text-center">
                        limite inferior
                    </th>
                    <th class="text-center">
                        limite superior
                    </th>
                    <th class="text-center">
                        Fecha
                    </th>
                </thead>
                <tbody>
                    @foreach($history as $register)
                    <tr>
                        <td class="text-center"
                            style="{{$register->value>$register->max || $register->value<$register->min ? 'color: red':'color:green'}}">
                            {{$register->value}}
                        </td>
                        <td class="text-center">
                            {{$register->min}}
                        </td>
                        <td class="text-center">{{$register->max}}

                        </td>
                        <td class="text-center">
                            {{$register->created_at}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
        </div>
    </center>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
        $('#history').DataTable();
        } );
        function getCurrTemp(){
            $.get( "/getCurrTemp", function( data ) {
                console.log(data);
                document.querySelector('#currTemp').innerHTML=data.value+"°";
                if(data.value<data.min || data.value>data.max)
                document.querySelector('#currTemp').style.color="red";
                else
                document.querySelector('#currTemp').style.color="green";

                document.querySelector('#minTemp').innerHTML=data.min+"°";
                document.querySelector('#maxTemp').innerHTML=data.max+"°";
                setTimeout("getCurrTemp()",1000);
            });

        }

        getCurrTemp();
        
        function getAvgTemp(){
        $.get( "/getAvgTemp", function( data ) {
        console.log(data);
        document.querySelector('#avgTemp').innerHTML=data+"°";
        setTimeout("getAvgTemp()",1000);
        });
        
        }
        
        getAvgTemp();
    </script>
</body>

</html>