<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'TempController@index');

Route::get('/getCurrTemp', 'TempController@getCurrTemp');

Route::get('/getAvgTemp', 'TempController@getAvgTemp');