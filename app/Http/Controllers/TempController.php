<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Temp;
use DB;

class TempController extends Controller
{
    public function index()
    {
        $history = Temp::orderBy('id', 'desc')->get();
        return view('welcome')->with(compact('history'));
    }
    public function getCurrTemp()
    {
        return Temp::orderBy('id', 'desc')->first();
    }
    public function getAvgTemp()
    {
        $avg = DB::select(DB::raw("SELECT AVG(value) as avg FROM temperatura WHERE  date(created_at) = curdate()"));
        return (int) $avg[0]->avg;
    }
}