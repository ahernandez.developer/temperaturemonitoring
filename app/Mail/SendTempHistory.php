<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendTempHistory extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $tempHistory;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tempHistory)
    {
        $this->tempHistory = $tempHistory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.sendTempHistory');
    }
}